<!DOCTYPE html>
<html>



<head>
  <title>Homepage</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="<?php echo esc_url(get_template_directory_uri()); ?>/images/favicon.ico" type="image/x-icon" />

  <link href="https://fonts.googleapis.com/css?family=Poppins:300,500,600,800,900&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,400&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/all.css">
  <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/slick.css">
  <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/humburger-menu.css">
  <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/aos.css">
  <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/style.css">
  <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/responsive.css">

</head>

<body>

  <header>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-8 col-sm-6 col-md-1">
          <div class="logo">
            <a href="<?php echo get_site_url(); ?>"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/header-logo.png" alt="Logo"></a>
          </div>
        </div>

        <div class="col-4 col-sm-6 col-md-11">
          <nav class="site-nav float-md-right" id="site-nav" role="navigation">
            <!-- MOBILE VIEW BUTTON -->
            <div class="nav-mobile">
              <i class="fa fa-bars show"></i>
              <i class="fa fa-close hide"></i>
            </div>

            <?php   wp_nav_menu( array(
		                                    'theme_location'    => 'primary',
		                                    'depth'             => 2,
		                                    'container'         => 'ul',
		                                    'container_class'   => '',
		                                    'container_id'      => '',
		                                    'menu_class'        => 'nav-off-canvas',
		                                    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
		                                    'walker'            => new WP_Bootstrap_Navwalker())
		                                );
		                                ?>
            <!-- <ul class="nav-off-canvas">
            
              <li><a class="active" href="index.html">Home</a>
              
              </li>
              <li><a class="dot-contact" href="physicians.html">Physicians</a></li>
              <li><a class="dot-contact" href="attorneys.html">Attorneys</a></li>
              <li><a class="dot-contact" href="patients.html">Patients</a></li>
              <li><a class="dot-contact" href="health-care-providers.html">Other Providers</a></li>
              <li><a class="dot-contact" href="contact-us.html">Contact Us</a></li>
              <li class="appointment"><a class="dot-contact " href="tel:+1234567890"><i class="fa fa-phone"></i>
                  +1234567890</a></li>
            </ul> -->
          </nav>
        </div>
      </div>
    </div>
  </header>