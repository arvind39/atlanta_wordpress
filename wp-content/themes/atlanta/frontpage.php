 <?php
  /* Template Name: Front Page*/

  get_header(); ?>

 <?php

  //Home Post 1
  $home_post1 = get_post(51);
  $home_post1_title = $home_post1->post_title;
  $home_post1_content = $home_post1->post_content;
  $home_post1_img = get_the_post_thumbnail_url($home_post1, 'thumbnail');

  //Home Post 2
  $home_post2 = get_post(54);
  $home_post2_title = $home_post2->post_title;
  $home_post2_content = $home_post2->post_content;
  $home_post2_img = get_the_post_thumbnail_url($home_post2, 'thumbnail');

  //Home Post 3
  $home_post3 = get_post(83);
  $home_post3_title = $home_post3->post_title;
  $home_post3_content = $home_post3->post_content;

  //Home Post 2
  $home_post4 = get_post(95);
  $home_post4_title = $home_post4->post_title;
  $home_post4_content = $home_post4->post_content;
  $home_post4_img = get_the_post_thumbnail_url($home_post4, 'thumbnail');

  ?>
 <section class="jumbotron m-0 pb-0 pl-0 pr-0">
   <div class="container">
     <div class="slider">
       <?php $posts = new WP_Query(array('post_type' => 'Slider', 'category_name' => 'slider', 'order' => 'ASC')); ?>
       <?php while ($posts->have_posts()) : $posts->the_post(); ?>
         <div class="slideShell">
           <article class="slideLeft">
             <h1 class="text-white"><?php echo get_the_title(); ?></h1>
           </article>

           <article class="slideRight">
             <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="Slide-1">
           </article>
         </div>
       <?php endwhile; ?>

       <!-- <div class="slideShell">
          <article class="slideLeft">
            <h1 class="text-white">Lorem Ipsum is simply dummy text of the printing</h1>
          </article>

          <article class="slideRight">
            <img src="<?php //echo esc_url( get_template_directory_uri() ); 
                      ?>/images/slider2.jpg" alt="Slide-2">
          </article>
        </div>

        <div class="slideShell">
          <article class="slideLeft">
            <h1 class="text-white">It has roots in a piece of classical Latin literature</h1>
          </article>

          <article class="slideRight">
            <img src="<?php //echo esc_url( get_template_directory_uri() ); 
                      ?>/images/slider3.jpg" alt="Slide-3">
          </article>
        </div> -->
     </div>
   </div>
 </section>

 <section class="aboutDiv">
   <div class="container">
     <div class="row">
       <div class="col-md-6">
         <article class="aboutLeft" data-aos="fade-right">
           <picture class="about-1 position-relative">
             <source srcset="<?php echo $home_post1_img; ?>">
             <img src="<?php echo $home_post1_img; ?>" alt="About-image-1">
           </picture>

           <!-- <picture class="about-2 position-absolute">
              <source srcset="<?php //echo esc_url( get_template_directory_uri() ); 
                              ?>/images/about-img2.jpg">
              <img src="<?php //echo esc_url( get_template_directory_uri() ); 
                        ?>/images/about-img2.jpg" alt="About-image-2">
            </picture> -->
         </article>
       </div>

       <div class="col-md-6">
         <article class="aboutRight pl-5 pr-5" data-aos="fade-left">
           <div class="headDiv pb-4">
             <h2 class="pb-5">
               <span class="text-uppercase">About Us</span> <?php echo $home_post1_title ?>
             </h2>
           </div>
           <div class="paraStyle"><?php echo $home_post1_content ?></div>



           <?php //echo $home_post1_content 
            ?>
           <a href="javascript:void(0)" class="text-white position-absolute view-more">View More</a>
         </article>
       </div>
     </div>
   </div>
 </section>

 <section class="applyDiv">
   <div class="container">
     <div class="row">
       <div class="col-md-6 pr-0">
         <article class="applyLeft position-relative" data-aos="fade-right">
           <picture>
             <source srcset="<?php echo esc_url(get_template_directory_uri()); ?>/images/apply-img.png">
             <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/apply-img.png" alt="Apply image">
           </picture>
           <div class="appTxt position-absolute">
             <h2 class="pb-4"><?php echo $home_post2_title ?></h2>
             <div class="pb-5"><?php echo $home_post2_content ?></div>
             <a href="#" class="d-inline-block text-white">Apply Here</a>
           </div>
         </article>
       </div>

       <div class="col-md-6 pl-0">
         <article class="applyRight pt-5 pb-5 pl-5 pr-5" data-aos="fade-left">

           <ul class="list-unstyled pt-4 pb-4 pl-3 pr-3">
             <?php $posts = new WP_Query(array('post_type' => 'home_page_post', 'category_name' => 'funding ', 'order' => 'ASC')); ?>
             <?php while ($posts->have_posts()) : $posts->the_post(); ?>
               <li class="text-white pl-5 pr-5 pb-5">
                 <picture class="float-left mr-4">
                   <source srcset="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>">
                   <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="Apply image">
                 </picture>
                 <p><?php echo get_the_title(); ?></p>
               </li>
             <?php endwhile; ?>


           </ul>
         </article>
       </div>
     </div>
   </div>
 </section>

 <section class="servDiv">
   <div class="container">
     <article class="servInn">
       <div class="headDiv pb-4" data-aos="fade-up">
         <h2 class="pb-5">
           <span class="text-uppercase">Services</span>
           <?php echo $home_post3_title ?>
         </h2>
       </div>
       <div class="paraStyle" data-aos="fade-up">
         <?php echo $home_post3_content ?>
       </div>
     </article>
   </div>
 </section>

 <section class="tabDiv pb-5 mb-5 position-relative">
   <div class="container">
     <article class="tabsInn" data-aos="fade-up">
       <ul class="nav nav-tabs border-0 mx-auto mb-4" role="tablist">
         <?php $posts = new WP_Query(array('post_type' => 'home_page_post', 'category_name' => 'services ', 'order' => 'ASC', 'posts_per_page' => 5)); ?>

         <?php $count = 1; ?>

         <?php while ($posts->have_posts()) : $posts->the_post(); ?>
           <?php if ($count == 1) { ?>
             <li class="nav-item">
               <a class="nav-link active" href="#service<?php echo $count; ?>" role="tab" data-toggle="tab"><?php echo get_the_title(); ?></a>
             </li>
           <?php } else { ?>
             <li class="nav-item">
               <a class="nav-link" href="#service<?php echo $count; ?>" role="tab" data-toggle="tab"><?php echo get_the_title(); ?></a>
             </li>
           <?php } ?>
           <?php $count++; ?>
         <?php endwhile; ?>
       </ul>

       <div class="tab-content pt-4">
         <?php $posts = new WP_Query(array('post_type' => 'home_page_post', 'category_name' => 'services ', 'order' => 'ASC', 'posts_per_page' => 5)); ?>

         <?php $count = 1; ?>

         <?php while ($posts->have_posts()) : $posts->the_post(); ?>

           <?php if ($count == 1) { ?>

             <div role="tabpanel" class="tab-pane fade in active show position-relative" id="service<?php echo $count; ?>">
               <div class="tabImg position-relative" data-aos="fade-right">
                 <picture class="mr-4">
                   <source srcset="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>">
                   <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="Physicians">
                 </picture>
               </div>
               <div class="tabTxt position-absolute bg-white" data-aos="fade-left">
                 <h2 class="mb-3"><?php echo get_the_title(); ?> <span class="mt-3"></span></h2>
                 <div class="paraStyle"><?php echo get_the_content(); ?></div>


                 <?php
                  $link = get_field('view_more');
                  if ($link) : ?>
                   <a href="<?php echo esc_url($link); ?>" class="text-white position-absolute">View More</a>

                 <?php endif; ?>

               </div>
             </div>

           <?php } else { ?>

             <div role="tabpanel" class="tab-pane fade position-relative" id="service<?php echo $count; ?>">
               <div class="tabImg position-relative">
                 <picture class="mr-4">
                   <source srcset="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>">
                   <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="Attorneys">
                 </picture>
               </div>

               <div class="tabTxt position-absolute bg-white">
                 <h2 class="mb-3"><?php echo get_the_title(); ?> <span class="mt-3"></span></h2>
                 <div class="paraStyle"><?php echo get_the_content(); ?></div>
                 <?php
                  $link = get_field('view_more');
                  if ($link) : ?>
                   <a href="<?php echo esc_url($link); ?>" class="text-white position-absolute">View More</a>

                 <?php endif; ?>

               </div>
             </div>

           <?php } ?>
           <?php $count++; ?>

         <?php endwhile; ?>

       </div>
     </article>
   </div>
 </section>

 <section class="blogDiv pb-5 mb-5">
   <div class="container">
     <div class="row align-items-center">
       <div class="col-md-6">
         <div class="blogLeft" data-aos="fade-right">
           <div class="headDiv pb-4">
             <h2 class="pb-5">
               <span class="text-uppercase">Blog</span> Latest from Our Blog
             </h2>

             <article class="blogTxt pt-5">
               <ul class="list-unstyled">
                 <?php $posts = new WP_Query(array('post_type' => 'post','category_name' => 'blog', 'order' => 'ASC', 'posts_per_page' => 2)); ?>

                 <?php while ($posts->have_posts()) : $posts->the_post(); ?>

                   <li class="position-relative d-flex align-items-center pb-5">
                    
                     <div class="txtLeft position-absolute">
                       <span class="date pl-2 pb-1 mb-1"><?php echo get_the_date('d'); ?></span>
                       <span class="month pl-2"><?php echo get_the_date('M'); ?></span>
                     </div>
                     <a href="<?php the_permalink(); ?>">
                     <div class="txtRight">
                       <span class="text-uppercase">By: <?php echo get_the_author_meta('display_name'); ?></span>                      
                       <p>
                       <?php echo wp_trim_words(get_the_title(), 12, '...'); ?>
                       
                       
                       <?php //echo get_the_title(); ?></p>
                     </div>
                     </a>
                   </li>

                 <?php endwhile; ?>

                 <!-- <li class="position-relative d-flex align-items-center">
                   <div class="txtLeft position-absolute">
                     <span class="date pl-2 pb-1 mb-1">03</span>
                     <span class="month pl-2">Oct</span>
                   </div>
                   <div class="txtRight">
                     <span class="text-uppercase">By: Admin</span>
                     <p>Labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
                   </div>
                 </li> -->
               </ul>
             </article>
           </div>
         </div>
       </div>

       <div class="col-md-6">
         <div class="newsRight" data-aos="fade-left">
           <h2><?php echo   $home_post4_title ?></h2>
           <div class="paraStyle mb-5"><?php echo   $home_post4_content ?></div>
           <!-- <form action="#" class="form-inline mt-5">
             <fieldset class="d-flex align-items-center">
               <div class="form-group mb-2 mr-3">
                 
                 <input type="password" class="form-control mr-3" id="inputPassword2" placeholder="Enter your email">
                 <button type="submit" class="btn text-white" value="Subscribe">Subscribe</button>
               </div>
             </fieldset>
           </form> -->
           <div class="subscribe_form">
           <?php echo do_shortcode('[contact-form-7 id="110" title="Subscribe"]');?>
           </div>
           
         </div>
       </div>
     </div>
   </div>
 </section>



 <?php get_footer(); ?>