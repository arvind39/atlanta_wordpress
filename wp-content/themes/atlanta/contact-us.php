<?php /* Template Name: Contact Page */
get_header();
?>

<?php

//Page Post 1
$page_post1 = get_post(108);
$page_post1_title = $page_post1->post_title;
$page_post1_content = $page_post1->post_content;
$page_post1_img = get_the_post_thumbnail_url($page_post1, 'thumbnail');

// //Page Post 2
// $page_post2 = get_post(107);
// $page_post2_title = $page_post2->post_title;
// $page_post2_content = $page_post2->post_content;
// $page_post2_img = get_the_post_thumbnail_url($page_post2, 'thumbnail');

?>
<!--Banner Part-->

<?php
$image_url = wp_get_attachment_url(get_post_thumbnail_id());
if (!empty(get_the_post_thumbnail())) {
?>
  <section class="atlanta_common" style="background-image:url('<?php echo $image_url; ?>">
    <div class="container">
      <div class="header_banner-cont">
        <div class="page-title-inner">
          <h2 class="page-title"><?php echo get_the_title(); ?></h2>
          <ul class="banner-breadcrumb">
            <li><a class="breadcrumb-entry pr-2" href="<?php echo get_site_url(); ?>">Home</a></li>
            <li><span class="breadcrumb-entry"><?php echo get_the_title(); ?></span></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
<?php } else { ?>
  <section class="atlanta_common" style="background-image:url('<?php echo esc_url(get_template_directory_uri()); ?>/images/attorney_banner.jpg');">
    <div class="container">
      <div class="header_banner-cont">
        <div class="page-title-inner">
          <h2 class="page-title"><?php echo get_the_title(); ?></h2>
          <ul class="banner-breadcrumb">
            <li><a class="breadcrumb-entry pr-2" href="<?php echo get_site_url(); ?>">Home</a></li>
            <li><span class="breadcrumb-entry"><?php echo get_the_title(); ?></span></li>
          </ul>
        </div>
      </div>
    </div>
  </section>

<?php } ?>

<!--Content Start-->

<section class="contactus-content-section">
  <div class="container">
    <div class="atorim_fullcondiv">
      <div class="row">
        <div class="col-md-5">
          <div class="left-side-contact">
            <div class="atorim_common_para getintouch-contact">
              <h3><?php echo $page_post1_title ?></h3>
              <div class="paraStyle"><?php echo $page_post1_content ?></div>
              <?php
                        if (is_active_sidebar('sidebar-2')) : //check the sidebar if used.
                          dynamic_sidebar('sidebar-2');  // show the sidebar.
                        endif;
                        ?>  
              <!-- <ul>
                <li><a href="tel:1234567890"><i class="fas fa-phone-square-alt"></i> +1234567890</a></li>
                <li><a href="mailto:abcd@email.com"><i class="fas fa-envelope-open-text"></i> abcd@email.com</a></li>
              </ul> -->
            </div>
            <div class="getintouch-form">

              <!-- <form>
                <div class="form-group">
                  <label for="inputName">Your Name</label>
                  <input type="text" class="form-control" id="inputName" placeholder="">
                </div>
                <div class="form-group">
                  <label for="inputEmail">Your Email</label>
                  <input type="email" class="form-control" id="inputEmail" placeholder="">
                </div>
                <div class="form-group">
                  <label for="inputPhone">Your Phone</label>
                  <input type="text" class="form-control" id="inputPhone" placeholder="">
                </div>
                <div class="form-group">
                  <label for="inputMessage">Your Message</label>
                  <textarea class="form-control" id="inputMessage" rows="5" placeholder=""></textarea>
                </div>
                <input type="submit" class="submit-btn" value="Submit">
              </form>  -->

              <?php echo do_shortcode('[contact-form-7 id="109" title="Contact form 1"]');?>

            </div>
          </div>
        </div>
        <div class="col-md-6 offset-md-1">
          <div class="getintouch-map">
            <h3>Our Location</h3>
            <div id="map" class="contact-map"></div>
            <script>
              function initMap() {
                // Create a new StyledMapType object, passing it an array of styles,
                // and the name to be displayed on the map type control.
                var styledMapType = new google.maps.StyledMapType(
                  [{
                      "elementType": "geometry",
                      "stylers": [{
                        "color": "#f5f5f5"
                      }]
                    },
                    {
                      "elementType": "labels.icon",
                      "stylers": [{
                        "visibility": "off"
                      }]
                    },
                    {
                      "elementType": "labels.text.fill",
                      "stylers": [{
                        "color": "#616161"
                      }]
                    },
                    {
                      "elementType": "labels.text.stroke",
                      "stylers": [{
                        "color": "#f5f5f5"
                      }]
                    },
                    {
                      "featureType": "administrative.land_parcel",
                      "elementType": "labels.text.fill",
                      "stylers": [{
                        "color": "#bdbdbd"
                      }]
                    },
                    {
                      "featureType": "poi",
                      "elementType": "geometry",
                      "stylers": [{
                        "color": "#eeeeee"
                      }]
                    },
                    {
                      "featureType": "poi",
                      "elementType": "labels.text.fill",
                      "stylers": [{
                        "color": "#757575"
                      }]
                    },
                    {
                      "featureType": "poi.park",
                      "elementType": "geometry",
                      "stylers": [{
                        "color": "#e5e5e5"
                      }]
                    },
                    {
                      "featureType": "poi.park",
                      "elementType": "labels.text.fill",
                      "stylers": [{
                        "color": "#9e9e9e"
                      }]
                    },
                    {
                      "featureType": "road",
                      "elementType": "geometry",
                      "stylers": [{
                        "color": "#ffffff"
                      }]
                    },
                    {
                      "featureType": "road.arterial",
                      "elementType": "labels.text.fill",
                      "stylers": [{
                        "color": "#757575"
                      }]
                    },
                    {
                      "featureType": "road.highway",
                      "elementType": "geometry",
                      "stylers": [{
                        "color": "#dadada"
                      }]
                    },
                    {
                      "featureType": "road.highway",
                      "elementType": "labels.text.fill",
                      "stylers": [{
                        "color": "#616161"
                      }]
                    },
                    {
                      "featureType": "road.local",
                      "elementType": "labels.text.fill",
                      "stylers": [{
                        "color": "#9e9e9e"
                      }]
                    },
                    {
                      "featureType": "transit.line",
                      "elementType": "geometry",
                      "stylers": [{
                        "color": "#e5e5e5"
                      }]
                    },
                    {
                      "featureType": "transit.station",
                      "elementType": "geometry",
                      "stylers": [{
                        "color": "#eeeeee"
                      }]
                    },
                    {
                      "featureType": "water",
                      "elementType": "geometry",
                      "stylers": [{
                        "color": "#c9c9c9"
                      }]
                    },
                    {
                      "featureType": "water",
                      "elementType": "labels.text.fill",
                      "stylers": [{
                        "color": "#9e9e9e"
                      }]
                    }
                  ], {
                    name: 'Styled Map'
                  });
                /*Create a Location Lat and Lng*/

                var locations = [
                  ['Colorado', 39.7837304, -100.4458825, 1]
                ];

                // Create a map object, and include the MapTypeId to add
                // to the map type control.
                var map = new google.maps.Map(document.getElementById('map'), {
                  center: {
                    lat: 39.7837304,
                    lng: -100.4458825
                  },
                  zoom: 4,
                  mapTypeControlOptions: {
                    mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain',
                      'styled_map'
                    ]
                  }
                });

                //Associate the styled map with the MapTypeId and set it to display.
                map.mapTypes.set('styled_map', styledMapType);
                map.setMapTypeId('styled_map');



                /*Pinned Location*/
                var infowindow = new google.maps.InfoWindow();
                var marker, i;
                for (i = 0; i < locations.length; i++) {
                  marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map
                  });

                  google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                      infowindow.setContent(locations[i][0]);
                      infowindow.open(map, marker);
                    }
                  })(marker, i));
                }
              }
            </script>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjHH2urSZPwU_jFK8czhCpHpRVE8HV1co&callback=initMap" async defer></script>