<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'atlanta' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'K1Xc=nu,9c:!(#k&44se*zqBhJiG@L0~yvuB-%s$n$RX#6i+he&I,B-H5!1(cIL3' );
define( 'SECURE_AUTH_KEY',  'y&)(S)dzM}CQWH=;#D,,8{hKRd[h|(V0sMkpHOeFHARgOxujnPO#wH_!22 quOZW' );
define( 'LOGGED_IN_KEY',    'kOPlg5[?n6UyewBAgQXXmk3>qr5bR;PzacZ1O[h!sv#VZpdR5GYj*&KQw rRJ6_o' );
define( 'NONCE_KEY',        'Xt5_;L,R o:St,+aj:C`y6:3V0ns=D cSN~`DPnbj)qXQXeB3Y._SaqNw8Ir~Z{C' );
define( 'AUTH_SALT',        '_ I=;WNx[AQM#u |dOI;e#9~9%M:3TX=Vx*O_P/`yJ$88{fNW}i_-Y$G:UV#pLc,' );
define( 'SECURE_AUTH_SALT', '0f=XdB?eKhN6K4Yz^Mw7sX2!#+=P!@5=NjXXv0wK?M[diP/4?>Kzh`9FJ6sF*rW3' );
define( 'LOGGED_IN_SALT',   '),DO1123Uh[a5C<1`_ 5Bwx1M0 ,oTagreM`H<p-zQ5[Zt/p}0KvdV/(HJO>,2vA' );
define( 'NONCE_SALT',       'OCYv&3%Vkn4:DJ`!kjXtPBW|}6dup:xPnEL:1o:=[4b8,TL{j@G #0h#BSBUw9a&' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
